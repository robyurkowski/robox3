module.exports = {
  //
  // REQUIRED
  //
  // The following plugins are required. It's highly recommended
  // that you keep them enabled unless you enjoy not running the bot.
  error: require('./plugins/error'),
  users: require('./plugins/users/users'),

  //
  // OPTIONAL
  //
  // The following plugins are entirely optional. Comment them out as you desire.
  calc:       require('./plugins/calc'),
  chance:     require('./plugins/chance'),
  convert:    require('./plugins/convert'),
  // greeter: require('./plugins/greeter.coffee'),
  slowphyte: require('./plugins/slowphyte'),
  time: require('./plugins/time'),
  wordWar: require('./plugins/word_war'),
  writingTracker: require('./plugins/writing_tracker'),
  // crier: require('./plugins/crier.coffee'),
  //

  //
  // NOT RECOMMENDED
  //
  // Be warned: uncommenting the following plugin puts you at a great deal of risk.
  // Don't do it unless you're absolutely sure you know what you're doing.
  // eval: require('./plugins/eval')
};
