/*jshint node: true */
// Robox 3: 3rd Generation IRC Bot
// Run in development using 'nodemon' with no args.
//"use strict";

var irc           = require('irc');
var _             = require('underscore');
var EventEmitter  = require('events').EventEmitter;
var RSVP          = require('rsvp');
var MongoClient   = require('mongodb').MongoClient;

var app;
var config        = require('./config');
var CommandParser = require('./lib/command_parser');

global.app = app = {
  plugins: {},
  config: config,
  vent: new EventEmitter(),
  bot: new irc.Client(config.server, config.botName, {
    channels: config.channels,
  }),
};


// Make the vent report any events reported through app.bot, too
app.vent.on('newListener', function(event, listener) {
  console.log("Added new listener for " +  event);
  app.bot.addListener(event, listener);
});


// Parse for commands
app.vent.on('message', function(nick, to, text, message) {
  var t = new CommandParser(text);
  if (t.isCommand) {
    app.vent.emit('command', t.command, nick, to, t);
  }
});


// Connect to database

var onDBConnectSuccess = function(db) {
  app.db = db;

  var registeredPlugins = require('./plugins');
  _.each(registeredPlugins, function(Klass, key) {
    app.plugins[key] = new Klass(app);
  });
};

var onDBConnectFailure = function(error) {
  console.log("ERROR: " +  error);
  process.exit(1);
};

RSVP.denodeify(MongoClient.connect)("mongodb://localhost:27017/robox").then(onDBConnectSuccess, onDBConnectFailure);
