###
  CompoundTimer allows multiple timers wrapped up in one

  The @times argument should be an object mapping start times -> timer length
  (all in milliseconds).

  Example: 
  If you want a timer to ring at midnight on every Monday, Wed, and Fri:
    @times =
      <next Mon, in milliseconds> : <1 week, in milliseconds>
      <next Wed, in milliseconds> : <1 week, in milliseconds>
      <next Fri, in milliseconds> : <1 week, in milliseconds>
###


class CompoundTimer
  constructor: (@times, @callback) ->
    # Ensure that all timers are in the future
    @advanceNextTime() while @timeUntil(@getNextTime()) < 0  
    @startTimer()

  timeUntil: (date) =>
    new Date(date).valueOf() - new Date().valueOf()

  getNextTime: =>
    Math.max 1000, Math.min.apply(null, t for t, dt of @times)

  advanceNextTime: =>
    t = @getNextTime()
    @times[t + @times[t]] = @times[t]
    delete @times[t]
    # todo: save JSON

  startTimer: =>
    setTimeout @finishTimer, @timeUntil(@getNextTime())

  finishTimer: =>
    @callback()
    @advanceNextTime()
    @startTimer()

 module.exports = CompoundTimer 
