CompoundTimer = require './crier/compoundtimer'

# Temporary replacement for JSON
# require './crier/cries'
cries =
  gunnerkrigg:
    msg:"Hear ye, hear ye! GUNNERKRIGG has updated! http://www.gunnerkrigg.com"    
    times: 
      1375081200000:604800000 # Start on Mon. 7/29/2013; run weekly
      1375254000000:604800000 # Start on Wed. 7/31/2013; run weekly
      1375426800000:604800000 # Start on Fri. 8/2/2013; run weekly

###
  test:
    msg:"Nothing updated - this is a test!"
    times:
      1374977100000:300000
      1374977160000:300000
###
class CrierPlugin
  constructor: (@app) ->
    @events = 
      'join': 'onJoin'
  
  onJoin: (channel, nick) =>
    return if nick != @app.bot.nick
    new CompoundTimer(item.times, (=> @app.bot.say channel, item.msg)) for id, item of cries

module.exports = CrierPlugin
