var Plugin          = require('../lib/plugin');
var WarRegistry     = require('./word_war/war_registry');
var EventManager    = require('./word_war/event_manager');
var util            = require('util');
var math            = require('mathjs');
if (typeof(math) === 'function') { math = math(); }

var _               = require('underscore');
var highlight       = _.partial(require('irc').colors.wrap, 'dark_red');


function WordWarPlugin(app) {
  Plugin.call(this, app);

  this.activeWordWars = [];
  this.registry = new WarRegistry(this.app);
  this.eventManager = new EventManager(this.app);
}

util.inherits(WordWarPlugin, Plugin);


WordWarPlugin.prototype.routes = {
  ww: {
    'new':      "createWordWar",
    'for':      "createWordWar",
    declare:    "createWordWar",
    join:       "joinWordWar",
    leave:      "leaveWordWar",
    who:        "listParticipants",
    list:       "listWordWars",
    cancel:     "cancelWordWar",
    status:     "statusOfWordWar",
    sign:       "signWordWar",
    help:       "displayHelp",
  },
};


WordWarPlugin.prototype.description =
  "A word war is a timed interval of writing time used by writers to " +
  "compete against each other in the effort to write the most words. After " +
  "a word war is declared, others have time to join. Then the war starts, and concludes " +
  "after a set duration. Then a reporting period follows, after which the winner is announced.";


WordWarPlugin.prototype.help = {
  "ww for <duration> in <delay> [sign <signingDelay>]": "creates a new word war. Aliases: " + highlight("new, declare."),
  "ww for <duration> at <HH:MM> [sign <signingDelay>]": "creates a new word war that begins at HH:MM. Aliases: " + highlight("new, declare"),
  "ww join [<creator>]": "joins an already-declared war",
  "ww leave [<creator>]": "leaves a war you've joined, cancelling it if you were the only person competing",
  "ww who [<creator>]": "lists participants in a war",
  "ww list": "lists all active word wars",
  "ww cancel [<creator>]": "cancels an active word war",
  "ww status [<creator>]": "shows status and remaining time in a word war",
  "ww sign <count>": "signs a war you've joined. Can also use an expression, i.e. " + highlight("!ww sign 502 - 300") + " for a count, and it will automatically be calculated",
  "ww help": "this text",
};


WordWarPlugin.prototype.createWordWar = function(nick, to, t) {
  this.registry.addWar({
    channel: to,
    creator: nick,
    duration: t.args[1],
    delay: t.args[3],
    signing: (t.args[5] || 5),
    participants: [nick],
  });
};


WordWarPlugin.prototype.joinWordWar = function(nick, to, t) {
  this.registry.addParticipant({
    nick: nick,
    channel: to,
    creator: t.args[1],
  });
};


WordWarPlugin.prototype.leaveWordWar = function(nick, to, t) {
  this.registry.removeParticipant({
    nick: nick,
    channel: to,
    creator: t.args[1],
  });
};


WordWarPlugin.prototype.listParticipants = function(nick, to, t) {
  this.registry.listParticipants({
    channel: to,
    creator: t.args[1],
  });
};


WordWarPlugin.prototype.listWordWars = function(nick, to, t) {
  this.registry.listWars({
    channel: to,
  });
};


WordWarPlugin.prototype.cancelWordWar = function(nick, to, t) {
  this.registry.cancelWar({
    channel: to,
    creator: nick, // Don't let other people than the creator cancel the war.
  });
};


WordWarPlugin.prototype.statusOfWordWar = function(nick, to, t) {
  this.registry.getStatus({
    channel: to,
    creator: t.args[1],
  });
};


WordWarPlugin.prototype.signWordWar = function(nick, to, t) {
  if (t.args.length < 2) {
    this.app.bot.say(to, "You have to say how many words you write! Try " + highlight('!ww sign <wordcount>') + ".");
  }

  else {
    var count = this.calc(t.args.slice(1).join(" "));
    this.registry.signWar({
      nick: nick,
      count: count,
      channel: to,
    });
  }
};


WordWarPlugin.prototype.calc = function(expr) {
  var val;

  try {
    val = math.eval(expr);
  }
  catch (error) {
    val = error.msg;
  }

  return val;
};


module.exports = WordWarPlugin;
