var _               = require('underscore');
var highlight       = _.partial(require('irc').colors.wrap, 'dark_red');
var RSVP            = require('rsvp');

function EventManager(app) {
  this.app = app;
  this.addListeners();
}


EventManager.prototype.listeners = {
  'wordwar:cancel':               'cancel',
  'wordwar:conclude:notWon':      'concludeNotWon',
  'wordwar:conclude:won':         'concludeWon',
  'wordwar:create:success':       'createSuccess',
  'wordwar:end':                  'end',
  'wordwar:join:repeat':          'joinRepeat',
  'wordwar:join:success':         'joinSuccess',
  'wordwar:leave:notApplicable':  'leaveNotApplicable',
  'wordwar:leave:success':        'leaveSuccess',
  'wordwar:list':                 'list',
  'wordwar:notFound':             'notFound',
  'wordwar:participants':         'participants',
  'wordwar:sign:notRegistered':   'signNotRegistered',
  'wordwar:sign:notYet':          'signNotYet',
  'wordwar:sign:success':         'signSuccess',
  'wordwar:start':                'start',
  'wordwar:status':               'status',
};


EventManager.prototype.addListeners = function() {
  var self = this;

  _.each(this.listeners, function(action, event) {
    self.app.vent.on(event, self[action].bind(self));
  });
};


EventManager.prototype.cancel = function(war, silent) {
  if (!silent) {
    this.app.bot.say(war.channel, "Cancelled word war.");
  }
};


EventManager.prototype.concludeNotWon = function(war) {
  this.app.bot.say(war.channel, "" + war.creator + "'s war ended. Nobody declared their count, so nobody wins. You all suck. Way to go.");
};


EventManager.prototype.concludeWon = function(war) {
  this.app.bot.say(war.channel, "" + war.creator + "'s war has concluded! Congratulations to everyone who participated, but special congratulations to " + war.winner + ", who won with " + war.winningWordCount + "!");
};


EventManager.prototype.createSuccess = function(war) {
  this.app.bot.say(war.channel, [
    war.creator,
    " has declared a ",
    war.duration,
    "-minute word war in ",
    war.channel,
    ", starting in ",
    war.delay,
    " minutes. Join by typing ",
    highlight('!ww join'),
    "."
  ].join(""));
};


EventManager.prototype.end = function(war) {
  this.app.bot.say(war.channel, "" + war.creator + "'s war has ended! You have " + war.signing + " minutes to declare your wordcount by typing " + highlight('!ww sign <wordcount>') + ". (Hint: you can add or subtract numbers instead of using a regular wordcount.) (cc " + Object.keys(war.participants).join(", ") + ")");
};


EventManager.prototype.joinRepeat = function(nick, to, war) {
  this.app.bot.say(to, "You're already participating in " + war.creator + "'s war.");
};


EventManager.prototype.joinSuccess = function(nick, to, war) {
  this.app.bot.say(to, "Joined " + war.creator + "'s war.");
};


EventManager.prototype.leaveNotApplicable = function(nick, to, war) {
  this.app.bot.say(to, "" + nick + ": you're not in that war. Maybe join with " + highlight("!ww join " + war.creator) + "?");
};


EventManager.prototype.leaveSuccess = function(nick, to, war) {
  this.app.bot.say(to, "Left the war.");
};


EventManager.prototype.list = function(to, number) {
  this.app.bot.say(to, "There are " + number + " war(s) in progress.");
};


EventManager.prototype.notFound = function(nick, to) {
  this.app.bot.say(to, "Couldn't find that word war.");
};


EventManager.prototype.participants = function(to, participants) {
  this.app.bot.say(to, "Participants: " + participants.join(', '));
};


EventManager.prototype.signNotRegistered = function(war, nick, count) {
  this.app.bot.say(war.channel, "" + nick + ": Don't you think you should register for that word war, first? Try " + highlight('!ww join') + ".");
};


EventManager.prototype.signNotYet = function(war, nick, count) {
  this.app.bot.say(war.channel, "" + nick + ": It's not time to count quite yet. Keep writing!");
};


EventManager.prototype.signSuccess = function(war, nick, count, oldCount) {
  var self = this;

  this.app.bot.say(war.channel, "" + nick + " has signed " + war.creator + "'s war: " + count + " words.");

  this.app.repositories.users.findOne({nick: nick}, function(error, user) {
    if (error) {
      self.app.bot.say(war.channel, "There was an error: " + error);
    }
    else {
      if (user) {
        var existingCount = (parseInt(user.get('wordCount'), 10) || 0);

        self.app.vent.once('user:changed', function(attrs) {
          self.app.bot.say(war.channel, "" + user.get('nick') + " has written a grand total of " + user.get('wordCount') + " words.");
        });

        user.set({wordCount: existingCount - oldCount + count});
      }
      else {
        var createUser = RSVP.denodeify(self.app.repositories.users.create);

        createUser({nick: nick, wordCount: count}).then(function(user) {
          self.app.bot.say(war.channel, "" + user.get('nick') + " has written a grand total of " + user.get('wordCount') + " words.");
        });
      }
    }
  });
};


EventManager.prototype.start = function(war) {
  this.app.bot.say(war.channel, "" + war.creator + "'s war is starting! You have " + war.duration + " minutes to write, so get cracking! (cc " + Object.keys(war.participants).join(", ") + ")");
};


EventManager.prototype.status = function(to, status) {
  this.app.bot.say(to, status);
};


module.exports = EventManager;
