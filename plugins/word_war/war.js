var EventEmitter  = require('events').EventEmitter;
var moment        = require('moment');
var util          = require('util');
var _             = require('underscore');

function War(app, registry, options) {
  EventEmitter.call(this);

  this.app = app;
  this.registry = registry;
  this.options = options;

  this.state = 'delaying';
  this.options.participants = {};
  this.options.participants[this.options.creator] = {wordCount: 0};
  this.calculateTimes();

  this.app.vent.emit('wordwar:create:success', this.options);
  this.timer = setTimeout(this.warIsStarting.bind(this), this.options.delay * 60 * 1000);
}

util.inherits(War, EventEmitter);


War.prototype.status = function() {
  var str = "" + this.options.creator + "'s war: ";

  switch (this.state) {
    case 'delaying':
      str += "starting in about " + this.timeRemaining(this.options.startTime) + ".";
      break;
    case 'progressing':
      str += "about " + this.timeRemaining(this.options.endTime) + " remaining.";
      break;
    case 'counting':
      str += "about " + this.timeRemaining(this.options.endCountTime) + " left to sign.";
      break;
    case 'concluding':
      str += "concluding.";
      break;
    default:
      return "Unknown state: " + this.state;
  }

  return str;
};


War.prototype.participants = function() {
  return Object.keys(this.options.participants);
};


War.prototype.addParticipant = function(nick) {
  if (this.participants().indexOf(nick) === -1) {
    this.options.participants[nick] = {wordCount: 0};
    return true;
  }
  else {
    return false;
  }
};


War.prototype.removeParticipant = function(nick) {
  if (this.participants().indexOf(nick) !== -1) {
    delete this.options.participants[nick];

    if (!this.participants().length) {
      this.cancel();
    }

    return true;
  }

  else {
    return false;
  }
};


War.prototype.sign = function(nick, count) {
  if (this.state === 'counting' && this.options.participants[nick]) {
    var oldCount = this.options.participants[nick].wordCount || 0;
    this.options.participants[nick].wordCount = count;
    this.app.vent.emit('wordwar:sign:success', this.options, nick, count, oldCount);
  }

  else {
    if (!this.options.participants[nick]) {
      this.app.vent.emit('wordwar:sign:notRegistered', this.options, nick, count);
    }

    else if (this.state !== 'counting') {
      this.app.vent.emit('wordwar:sign:notYet', this.options, nick, count);
    }
  }
};


War.prototype.cancel = function(opts) {
  if (typeof(opts) === 'undefined') { opts = {silent: false}; }

  this.state = 'cancelling';
  this.app.vent.emit('wordwar:cancel', this.options, opts.silent);

  if (this.timer) {
    clearTimeout(this.timer);
  }

  return true;
};


// Called when the war's delay has elapsed.
War.prototype.warIsStarting = function() {
  this.state = 'progressing';
  this.app.vent.emit('wordwar:start', this.options);
  this.timer = setTimeout(this.warIsEnding.bind(this), this.options.duration * 60 * 1000);
};


// Called when the war's duration has elapsed.
War.prototype.warIsEnding = function() {
  this.state = 'counting';
  this.app.vent.emit('wordwar:end', this.options);
  this.timer = setTimeout(this.warIsConcluding.bind(this), this.options.signing * 60 * 1000);
};


// Called when everything has been tallied or time has run out in the signatory period.
War.prototype.warIsConcluding = function() {
  this.state = 'concluding';
  this.calculateWinner();
  this.app.vent.emit('wordwar:conclude', this.options);
};


War.prototype.calculateTimes = function() {
  this.options.declareTime    = moment();
  this.options.startTime      = this.options.declareTime.clone().add('minutes', this.options.delay);
  this.options.endTime        = this.options.startTime.clone().add('minutes', this.options.duration);
  this.options.endCountTime   = this.options.endTime.clone().add('minutes', this.options.signing);
};


War.prototype.timeRemaining = function(timeUntil) {
  return timeUntil.fromNow(true);
};


War.prototype.calculateWinner = function() {
  var winningWordCount = 0;
  var winningNick;

  _.each(this.options.participants, function(data, nick) {
    if (data.wordCount > winningWordCount) {
      winningWordCount = data.wordCount;
      winningNick = nick;
    }
  });

  if (winningNick) {
    this.options.winner = winningNick;
    this.options.winningWordCount = winningWordCount;
    this.app.vent.emit('wordwar:conclude:won', this.options);
  }

  else {
    this.app.vent.emit('wordwar:conclude:notWon', this.options);
  }
};


module.exports = War;
