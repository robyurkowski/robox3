var EventEmitter = require('events').EventEmitter;
var moment        = require('moment');
var util = require('util');
var War = require('./war');

function WarRegistry(app) {
  EventEmitter.call(this);

  this.app = app;
  this.wars = [];
  this.app.vent.on('wordwar:cancel', function(war, silent) {
    delete this.wars[war.channel][war.creator];

    if (!Object.keys(this.wars[war.channel]).length) {
      delete this.wars[war.channel];
    }
  }.bind(this));

  this.app.vent.on('wordwar:conclude', function(args) {
    var war = this.getWar(args.channel, args.creator);
    if (war) {
      war.cancel({silent: true});
    }
  }.bind(this));
}

util.inherits(WarRegistry, EventEmitter);


WarRegistry.prototype.addParticipant = function(args) {
  var war = this.getWar(args.channel, args.creator);
  if (!war) {
    this.app.vent.emit('wordwar:notFound', args.nick, args.channel);
    return false;
  }

  else {
    var event = war.addParticipant(args.nick) ? 'wordwar:join:success' : 'wordwar:join:repeat';
    this.app.vent.emit(event, args.nick, args.channel, war.options);
  }
};


WarRegistry.prototype.removeParticipant = function(args) {
  var war = this.getWar(args.channel, args.creator);
  if (!war) {
    this.app.vent.emit('wordwar:notFound', args.nick, args.channel);
    return false;
  }

  else {
    var event = war.removeParticipant(args.nick) ? 'wordwar:leave:success' : 'wordwar:leave:notApplicable';
    this.app.vent.emit(event, args.nick, args.channel, war.options);
  }
};


WarRegistry.prototype.listParticipants = function(args) {
  var war = this.getWar(args.channel, args.creator);
  if (!war) {
    this.app.vent.emit('wordwar:notFound', args.nick, args.channel);
    return false;
  }

  else {
    this.app.vent.emit('wordwar:participants', args.channel, war.participants());
  }
};


WarRegistry.prototype.getStatus = function(args) {
  var war = this.getWar(args.channel, args.creator);
  if (!war) {
    this.app.vent.emit('wordwar:notFound', args.nick, args.channel);
    return false;
  }

  else {
    this.app.vent.emit('wordwar:status', args.channel, war.status());
  }
};


WarRegistry.prototype.listWars = function(args) {
  this.app.vent.emit('wordwar:list', args.channel, this.length());
};


WarRegistry.prototype.getWar = function(channel, creator) {
  if (typeof(creator) !== 'undefined') {
    return this.wars[channel] && this.wars[channel][creator];
  }

  else if (this.wars[channel] && Object.keys(this.wars[channel]).length === 1) {
    var key = Object.keys(this.wars[channel])[0];
    return this.wars[channel][key];
  }

  else {
    return false;
  }
};


WarRegistry.prototype.addWar = function(args) {
  var matches = args.delay.match(/(\d{1,2}):(\d{1,2})/i);

  if (matches) {
    var now = moment();
    var timeOfStart = moment();
    timeOfStart.hours(matches[1]);
    timeOfStart.minutes(matches[2]);
    if (timeOfStart.diff(now, 'minutes', true) < 0) { timeOfStart.add('d', 1); }
    args.delay = timeOfStart.diff(now, 'minutes', true);
  }

  this.wars[args.channel] = this.wars[args.channel] || [];
  this.wars[args.channel][args.creator] = new War(this.app, this, args);
};


WarRegistry.prototype.cancelWar = function(args) {
  var war = this.getWar(args.channel, args.creator);
  if (!war) {
    this.app.vent.emit('wordwar:notFound', args.nick, args.channel);
    return false;
  }

  else {
    war.cancel();
  }
};


WarRegistry.prototype.signWar = function(args) {
  var war = this.getWar(args.channel, args.creator);
  if (!war) {
    this.app.vent.emit('wordwar:notFound', args.nick, args.channel);
    return false;
  }

  else {
    war.sign(args.nick, args.count);
  }
};


WarRegistry.prototype.length = function() {
  return Object.keys(this.wars).length;
};


module.exports = WarRegistry;
