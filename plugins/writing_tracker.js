/* This plugin does the following:
 * - Expands the result of signing a word war to append to a running count of the day's total
 * - Tracks daily word counts to maintain a log of writing for each of its users
*/

var Plugin          = require('../lib/plugin');
var util            = require('util');
var _               = require('underscore');
var highlight       = _.partial(require('irc').colors.wrap, 'dark_red');

function wtp(app) {
  Plugin.call(this, app);
}

util.inherits(wtp, Plugin);

wtp.prototype.routes = {
  wc: {
    'set': "setWordCount",
    'help': "displayHelp",
  },
};

wtp.prototype.description = "Manages current and historical word counts for registered users.";

wtp.prototype.help = {
  "wc set <words>": "sets your wordcount to the value specified",
  "wc help": "this help message",
};


wtp.prototype.setWordCount = function(nick, to, t) {
  if (t.args.length > 1) {
    var notifyOfWordCountChange = function(user) {
      this.app.bot.say(to, "Set " + user.get('nick') + "'s word count to " + user.get('wordCount') + ".");
    };

    var updateWordCount = function(user) {
      var wordCount = t.args[1];
      user.set('wordCount', wordCount);
      return this.app.repositories.users.update(user.attributes).then(notifyOfWordCountChange.bind(this, user));
    };

    var promise = this.app.repositories.users.findOrCreate({nick: nick});
    return promise.then(updateWordCount.bind(this));
  }

  else {
    this.app.bot.say(to, "You must give a word count.");
  }
};

module.exports = wtp;


/*
 *
 *
 *
*/
