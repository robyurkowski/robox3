var Plugin  = require('../lib/plugin');
var math    = require('mathjs');
if (typeof(math) === 'function') { math = math(); }
var util    = require('util');

function CalcPlugin(app) {
  Plugin.call(this, app);
}

util.inherits(CalcPlugin, Plugin);

CalcPlugin.prototype.onCommand = function(command, nick, to, t) {
  switch (command) {
    case 'calc':
      this.app.bot.say(to, "" + nick + ": " + this.calc(t.argText));
      break;
  }
};

CalcPlugin.prototype.calc = function(expr) {
  var val;

  try {
    val = math.eval(expr);
  }
  catch (error) {
    val = error.msg;
  }

  console.log('math.eval: ' + math.eval(expr));

  return val;
};

module.exports = CalcPlugin;
