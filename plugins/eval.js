var Plugin = require('../lib/plugin');
var util = require('util');

function EvalPlugin(app) {
  Plugin.call(this, app);
}

util.inherits(EvalPlugin, Plugin);


EvalPlugin.prototype.routes = {
  eval: "eval",
};


EvalPlugin.prototype.eval = function(nick, to, t) {
  try {
    this.app.bot.say(to, eval(t.argText));
    console.log(">> ", t.argText);
  }

  catch (error) {
    this.app.bot.say(to, "ERROR: " + error.message);
  }
};


module.exports = EvalPlugin;
