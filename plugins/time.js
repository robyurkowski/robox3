var Plugin = require('../lib/plugin');
var util = require('util');
var moment = require('moment');
var _ = require('underscore');
var highlight = _.partial(require('irc').colors.wrap, 'dark_red');

function TimePlugin(app) {
  Plugin.call(this, app);

}

util.inherits(TimePlugin, Plugin);

TimePlugin.prototype.routes = {
  time: "displayTime",
};


TimePlugin.prototype.displayTime = function(nick, to, t) {
  this.app.bot.say(to, "" + nick + ": the current time is " + highlight(moment().format()) + ".");
};


module.exports = TimePlugin;
