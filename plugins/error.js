var util = require('util');

function ErrorPlugin(app) {
  this.app = app;

  this.app.vent.on('error', function(message) {
    console.error("[ERROR]", message);
  });
}


module.exports = ErrorPlugin
