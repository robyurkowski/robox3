var _ = require('underscore');

function User(attributes, app) {
  this.attributes = attributes;
  this.app = app;
  this.nick = this.attributes.nick;
}


User.prototype.toHash = function() {
  return this.attributes;
};


User.prototype.except = function(excludes) {
  if (typeof(excludes) === 'undefined') { excludes = []; }

  var output = [];
  _.each(this.attributes, function(value, key) {
    if (excludes.indexOf(key) === -1) {
      output.push("" + key + ": " + value);
    }
  });

  return output.join(", ");
};


User.prototype.get = function(attr) {
  return this.attributes[attr];
};


User.prototype.set = function(key, value) {
  if (typeof(key) === 'object') {
    this.attributes = _.extend(this.attributes, key);
  }

  else {
    this.attributes[key] = value;
  }
};


User.prototype.clear = function(keys) {
  if (typeof(keys) === 'undefined') { keys = []; }

  var self = this;

  _.each(keys, function(key) { delete self.attributes[key]; });
};


User.prototype.hasAttribute = function(key) {
  return (Object.keys(this.attributes).indexOf(key) !== -1);
};


module.exports = User;
