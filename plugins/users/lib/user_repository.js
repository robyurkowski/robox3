var User = require('./user');
var RSVP = require('rsvp');
var util = require('util');


function UserRepository(app) {
  this.app = app;
  this.clx = this.app.db.collection('users');
}


UserRepository.prototype.find = function(attrs, projections) {
  if (typeof(projections) === 'undefined') { projections = null; }

  var self = this;
  var findAll = RSVP.denodeify(this.clx.find(attrs, projections).toArray.bind(this.clx));
  var mapToUsers = function(results) { return results.map(function(result) { return new User(result, self.app); }); };

  return findAll().then(mapToUsers);
};


UserRepository.prototype.findOne = function(attrs) {
  var self = this;
  var mapToUser = function(result) { return new User(result, self.app); };
  var findOne = RSVP.denodeify(this.clx.findOne.bind(this.clx));
  var promise = findOne(attrs, {});
  return promise.then(mapToUser);
};


UserRepository.prototype.create = function(attrs) {
  var user = new User(attrs, this.app);
  var insert = RSVP.denodeify(this.clx.insert.bind(this.clx));

  return insert(user.toHash(), {w: 1});
};


UserRepository.prototype.findOrCreate = function(attrs) {
  var self = this;
  var findOne = RSVP.denodeify(this.clx.findOne.bind(this.clx));
  var resultReturned = function(result) { 
    if (result) {
      return new User(result);
    }
    else {
      return this.create(attrs).then(function(users) { return new User(users[0]); });
    }
  };
  return findOne(attrs, {}).then(resultReturned.bind(this));
};


UserRepository.prototype.update = function(attrs) {
  var update = RSVP.denodeify(this.clx.update.bind(this.clx));

  return update({_id: attrs._id}, attrs, {w: 1});
};


UserRepository.prototype.remove = function(attrs) {
  var self = this;
  var remove = RSVP.denodeify(this.clx.remove.bind(this.clx));

  return remove(attrs, true);
};


UserRepository.prototype.truncate = function() {
  var self = this;
  var remove = RSVP.denodeify(this.clx.remove.bind(this.clx));

  return remove();
};


UserRepository.prototype.count = function(attrs) {
  var self = this;
  var count = RSVP.denodeify(this.clx.find(attrs).count.bind(this.clx));

  return count();
};


module.exports = UserRepository;
