var Plugin = require('../../lib/plugin');
var UserRepository = require('./lib/user_repository');
var _ = require('underscore');
var highlight = _.partial(require('irc').colors.wrap, 'dark_red');
var util = require('util');
var math = require('mathjs');
if (typeof(math) === 'function') { math = math(); }


function UsersPlugin(app) {
  Plugin.call(this, app);

  this.app = app;
  this.app.repositories = this.app.repositories || {};
  this.app.repositories.users = new UserRepository(this.app);
}
util.inherits(UsersPlugin, Plugin);


UsersPlugin.prototype.routes = {
  users: {
    list:     "listUsers",
    count:    "countUsers",
    truncate: "truncateUsers",
    create:   "createUser",
    remove:   "removeUser",
    show:     "showUser",
    get:      "getAttributeFromUser",
    set:      "addAttributeToUser",
    unset:    "removeAttributeFromUser",
    update:   "updateAttributeOnUser",
    help:     "displayHelp",
  },
};


UsersPlugin.prototype.description = "The `users` plugin is used to provide broad support " +
                                    "across Robox for a variety of tracking needs. It's " +
                                    "considered a prerequisite for quite a few different " +
                                    "plugins. The plugin also provides a simple querying " +
                                    "interface for viewing the stored data about a " +
                                    "user.";


UsersPlugin.prototype.help = {
  "users list": "lists all users the bot remembers.",
  "users count": "gives the count of all users in the bot's memory",
  "users truncate": "completely clears all users from the bot's memory",
  "users create <nickname>": "register a user going by <nickname>",
  "users remove <nickname>": "removes a user going by <nickname> from the bot's memory",
  "users show <nickname>": "shows all stored values on the user going by <nickname>",
  "users get <nickname> <key>": "shows just the value for <key> stored on the user going by <nickname>",
  "users set <nickname> <key> <value ...>": "stores an arbitrary value as <key> on the user going by <nickname>",
  "users update <nickname> <key> <value>": "adds a value to a key. If the existing value is a number, increments or decrements; if a string, appends.",
  "users unset <nickname> <key>": "removes the specified key and its data from the user going by <nickname>",
  "users help": "this text",
};


UsersPlugin.prototype.createUser = function(nick, to, t) {
  if (t.args.length > 1) {
    var self = this;

    this.app.repositories.users.findOrCreate({nick: t.args[1]})
      .then(function(user) {
        self.app.bot.say(to, "Okay, user " + user.nick + " exists.");
      })
      .catch(this.errorOccurred.bind(this, to));
  }

  else {
    this.app.bot.say(to, "You must specify a nickname.");
  }
};


UsersPlugin.prototype.showUser = function(nick, to, t) {
  if (t.args.length > 1) {
    var self = this;

    this.app.repositories.users.findOne({nick: t.args[1]})
      .then(function(user) {
        self.app.bot.say(to, user.except(['_id']));
      })
      .catch(this.errorOccurred.bind(this, to));
  }

  else {
    this.app.bot.say(to, "You must specify a nickname.");
  }
};


UsersPlugin.prototype.removeUser = function(nick, to, t) {
  if (t.args.length > 1) {
    var self = this;

    this.app.repositories.users.remove({nick: t.args[1]})
      .then(function(count) {
        self.app.bot.say(to, "Removed " + count + " user(s).");
      })
      .catch(this.errorOccurred.bind(this, to));
  }

  else {
    this.app.bot.say(to, "You must specify a nickname.");
  }
};


UsersPlugin.prototype.truncateUsers = function(nick, to, t) {
  var self = this;

  this.app.repositories.users.truncate()
    .then(function(count) {
      self.app.bot.say(to, "Truncated table, removing " + count + " records.");
    })
    .catch(this.errorOccurred.bind(this, to));
};


UsersPlugin.prototype.countUsers = function(nick, to, t) {
  var self = this;

  this.app.repositories.users.count(null)
    .then(function(count) {
      self.app.bot.say(to, "There are " + count + " users.");
    })
    .catch(this.errorOccurred.bind(this, to));
};


UsersPlugin.prototype.listUsers = function(nick, to, t) {
  var self = this;

  this.app.repositories.users.find({}, {nick: 1})
    .then(function(users) {
      self.app.bot.say(to, "Users: " + users.map(function(user) { return user.nick; }).join(", "));
    })

    .catch(this.errorOccurred.bind(this, to));
};


UsersPlugin.prototype.getAttributeFromUser = function(nick, to, t) {
  if (t.args.length > 2) {
    var self = this;

    this.app.repositories.users.findOne({nick: t.args[1]})
      .then(function(user) {
        self.app.bot.say(to, "" + t.args[2] + ": " + user.get(t.args[2]));
      })
      .catch(this.errorOccurred.bind(this, to));
  }

  else {
    this.app.bot.say(to, "You must specify a nickname and a field name.");
  }
};


UsersPlugin.prototype.addAttributeToUser = function(nick, to, t) {
  if (t.args.length > 3) {
    var self = this;

    this.app.repositories.users.findOne({nick: t.args[1]})
      .then(function(user) {
        user.set(t.args[2], t.args.slice(3).join(" "));
        return this.app.repositories.users.update(user.attributes);
      })

      .then(function(result) {
        self.app.bot.say(to, "Updated user " + t.args[1] + ".");
      })

      .catch(this.errorOccurred.bind(this, to));
  }

  else {
    this.app.bot.say(to, "You must specify a nickname, a field name, and a value.");
  }
};


UsersPlugin.prototype.updateAttributeOnUser = function(nick, to, t) {
  if (t.args.length > 3) {
    var self = this;
    this.app.repositories.users.findOne({nick: t.args[1]})
      .then(function(user) {
        var existingValue = user.get(t.args[2]);
        var replacementValue;

        if (existingValue.match(/\d+/)) {
          replacementValue = "" + math.eval("" + existingValue + " + " + t.args.slice(3));
        }

        else {
          replacementValue = existingValue + t.args.slice(3);
        }

        user.set(t.args[2], replacementValue);
        return this.app.repositories.users.update(user.attributes);
      })

      .then(function(result) {
        self.app.bot.say(to, "Updated user " + t.args[1] + ".");
      })

      .catch(this.errorOccurred.bind(this, to));
  }

  else {
    this.app.bot.say(to, "You must specify a nickname, a field name, and a value.");
  }
};


UsersPlugin.prototype.removeAttributeFromUser = function(nick, to, t) {
  if (t.args.length > 2) {
    var self = this;

    this.app.repositories.users.findOne({nick: t.args[1]})
      .then(function(user) {
        user.clear([t.args[2]]);
        return this.app.repositories.users.update(user.attributes);
      })

      .then(function(result) {
        self.app.bot.say(to, "Updated user " + t.args[1] + ".");
      })

      .catch(this.errorOccurred.bind(this, to));
  }

  else {
    this.app.bot.say(to, "You must specify a nickname and a field name.");
  }
};


UsersPlugin.prototype.errorOccurred = function(to, error) {
  this.app.bot.say(to, "There was an error: " + error);
};

module.exports = UsersPlugin;
