var Plugin = require('../lib/plugin');
var util = require('util');
var randomInt = require('../lib/robox_utils').randomInt;

function SlowphytePlugin(app) {
  Plugin.call(this, app);
}

util.inherits(SlowphytePlugin, Plugin);

SlowphytePlugin.prototype.initialize = function() {
  this.app.vent.on('message', this.onMessage.bind(this));
};

SlowphytePlugin.prototype.onMessage = function(nick, to, text, message) {
  var hilarityPreservationCoefficient = randomInt(1, 8);
  if (text.match(/lophyte/i) && hilarityPreservationCoefficient === 1) {
    this.app.bot.say(to, "slowphyte*");
  }
};

module.exports = SlowphytePlugin;
