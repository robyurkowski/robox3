var Plugin          = require('../lib/plugin');
var _               = require('underscore');
var highlight       = _.partial(require('irc').colors.wrap, 'dark_red');
var util            = require('util');


function ConvertPlugin(app) {
  Plugin.call(this, app);
};

util.inherits(ConvertPlugin, Plugin);


ConvertPlugin.prototype.routes = {
  convert: "convert",

};


ConvertPlugin.prototype.convert = function(nick, to, t) {
  var matcher = "(-?\\d+) (" + this.matchUnits + ") to (" + this.matchUnits + ")";
  var validCommandFormat = new RegExp(matcher, 'i');

  if (t.args.length === 4 && t.argText.match(validCommandFormat)) {
    this.receivedValidlyFormattedCommand(nick, to, t);
  }

  else {
    this.app.bot.say(to, "" + nick + ": Try it like " + highlight('!convert 50 c to f') +
                         ", all lowercase, and with a space between quantity and unit. " +
                         "I support the following units: " + this.matchUnits.split("|").join(", "));
  }

};


ConvertPlugin.prototype.receivedValidlyFormattedCommand = function(nick, to, t) {
  var methodName = t.args[1] + "_to_" + t.args[3];
  if (this[methodName]) {
    var result = this[methodName](t.args[0]);
    this.app.bot.say(to, "" + nick + ": " + t.args[0] + " " + t.args[1] + " = " + result + " " + t.args[3]);
  }

  else {
    this.app.bot.say(to, "" + nick + ": That conversion doesn't exist. Here are the formats I support: " +
                         this.matchUnits.split("|").join(", "));
  }
};


ConvertPlugin.prototype.matchUnits = "c|f";


ConvertPlugin.prototype.c_to_f = function(val) {
  return (Math.floor(val * 1.8 + 32));
};


ConvertPlugin.prototype.f_to_c = function(val) {
  return (Math.floor((val - 32) / 1.8));
};


module.exports = ConvertPlugin;
