var Plugin = require('../lib/plugin');
var _ = require('underscore');
var util = require('util');
var highlight = _.partial(require('irc').colors.wrap, 'dark_red');
var randomInt = require('../lib/robox_utils').randomInt;


function ChancePlugin(app) {
  Plugin.call(this, app);
}

util.inherits(ChancePlugin, Plugin);


ChancePlugin.prototype.routes = {
  toss: "flipCoin",
  which: "decide",
  decide: "decide",
  roll: "roll",
};


ChancePlugin.prototype.flipCoin = function(nick, to, t) {
  this.app.bot.say(to, "" + nick + ": " + this.doAOrBChoice());
};


ChancePlugin.prototype.doAOrBChoice = function(heads, tails) {
  if (typeof(heads) === 'undefined') { heads = "Heads"; }
  if (typeof(tails) === 'undefined') { tails = "Tails"; }

  return (randomInt(0, 1) ? heads : tails);
};


ChancePlugin.prototype.decide = function(nick, to, t) {
  if (t.args.length === 3) {
    this.app.bot.say(to, "" + nick + ": " + this.doAOrBChoice(t.args[0], t.args[2]));
  }

  else {
    this.app.bot.say(to, "" + nick + ": Try it like " + highlight("!decide pizza or chinese") + ".");
  }
};


ChancePlugin.prototype.roll = function(nick, to, t) {
  if (t.args.length === 1 && (matches = t.args[0].match(/^(\d+)d(\d+)$/i))) {
    var dice = matches[1];
    var sides = matches[2];
    var results = [];
    var self = this;

    _(dice).times(function(i) { results.push(randomInt(1, sides)); });
    var sum = results.reduce(function(sum, val) { return sum + val; });

    this.app.bot.say(to, "" + nick + ": " + sum + ": " + results.join(", "));
  }

  else {
    this.app.bot.say(to, "" + nick + ": Try it like " + highlight("!roll 1d6") + ".");
  }
};


module.exports = ChancePlugin;
