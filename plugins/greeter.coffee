class GreeterPlugin
  constructor: (@app) ->
    @app.vent.on 'join', @greet

  greet: (channel, nick, message) =>
    @app.bot.say channel, "Hello, #{nick}!" if nick != @app.bot.opt.nick

module.exports = GreeterPlugin
