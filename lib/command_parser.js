var CommandParser = function(text) {
  this.text = text;
  this.isCommand = (text.indexOf("!") === 0);

  if (this.isCommand) {
    var args = this.text.substring(1).split(" ");
    this.command = args.shift();
    this.args = args;
    this.argText = args.join(" ");
  }

};

module.exports = CommandParser;
