var EventEmitter    = require('events').EventEmitter;
var _               = require('underscore');
var highlight       = _.partial(require('irc').colors.wrap, 'dark_red');
var util            = require('util');


function Plugin(app) {
  EventEmitter.call(this);

  this.app = app;
  this.initialize();
};

util.inherits(Plugin, EventEmitter);

Plugin.prototype.initialize = function() {
  this.app.vent.on('command', this.onCommand.bind(this));
};


Plugin.prototype.onCommand = function(command, nick, to, t) {
  var action;

  if (this.routes && (rootCommand = this.routes[command])) {
    if (typeof(rootCommand) === 'object') {
      if ((action = this.routes[command][t.args[0]]) && this[action]) {
        return this[action](nick, to, t);
      }

      else {
        return this.commandDoesNotExist(nick, to, t);
      }
    }

    else {
      return this[rootCommand](nick, to, t);
    }
  }
};


Plugin.prototype.displayHelp = function(nick, to, t) {
  this.app.bot.notice(nick, this.description);
  _.each(this.help, function(message, command) {
    this.app.bot.notice(nick, highlight("!" + command) + " -- " + message);
  });
};


Plugin.prototype.commandDoesNotExist = function(nick, to, t) {
  var commandHelp = "!" + t.command + " help";
  this.app.bot.say(to, "" + nick + ": That command doesn't exist. Try " + highlight(commandHelp) + " for help.");
};


module.exports = Plugin
