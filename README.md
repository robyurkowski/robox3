# Robox3

## Requirements:

- [MongoDB installed](http://docs.mongodb.org/manual/tutorial/install-mongodb-on-ubuntu/)
- `nodemon` and `coffee` installed globally (`sudo npm install -g nodemon && sudo npm install -g coffee`)

## To Run:

- Install node packages
- Copy `config.js_example` to `config.js` and edit
- Run using `nodemon`
